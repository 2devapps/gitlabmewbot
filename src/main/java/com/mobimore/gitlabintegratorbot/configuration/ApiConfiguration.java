package com.mobimore.gitlabintegratorbot.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApiConfiguration {

    @Value("${api.protocol}")
    private String apiProtocol;

    @Value("${api.url}")
    private String apiUrl;

    @Value("${api.port}")
    private String apiPort;

    public String getApiProtocol() {
        return apiProtocol;
    }
    public void setApiProtocol(String apiProtocol) {
        this.apiProtocol = apiProtocol;
    }

    public String getApiUrl() {
        return apiUrl;
    }
    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getFullApiUrl() {
        return apiProtocol + "://" + apiUrl + ":" + apiPort + "/api/gitlab/";
    }
}
