package com.mobimore.gitlabintegratorbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabIntegratorBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabIntegratorBotApplication.class, args);
	}

}
