package com.mobimore.gitlabintegratorbot.utils;

public class HTML {
    private final StringBuilder stringBuilder = new StringBuilder();

    public HTML a(String url, String title) {
        stringBuilder.append("<a href=\"").append(url).append("\">").append(title).append("</a>");
        return this;
    }

    public HTML b(String text) {
        stringBuilder.append("<b>").append(text).append("</b>");
        return this;
    }

    public HTML text(String text) {
        stringBuilder.append(text);
        return this;
    }

    @Override
    public String toString() {
        return stringBuilder.toString();
    }
}
