package com.mobimore.gitlabintegratorbot.gitlab.api.pipelineEvent;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class ObjectAttributes{
    public int id;
    public String ref;
    public boolean tag;
    public String sha;
    @JsonProperty("before_sha")
    public String beforeSha;
    public String source;
    public String status;
    public ArrayList<String> stages;
    @JsonProperty("created_at")
    public String createdAt;
    @JsonProperty("finished_at")
    public String finishedAt;
    public int duration;
    public ArrayList<Variable> variables;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getRef() {
        return ref;
    }
    public void setRef(String ref) {
        this.ref = ref;
    }

    public boolean isTag() {
        return tag;
    }
    public void setTag(boolean tag) {
        this.tag = tag;
    }

    public String getSha() {
        return sha;
    }
    public void setSha(String sha) {
        this.sha = sha;
    }

    public String getBeforeSha() {
        return beforeSha;
    }
    public void setBeforeSha(String beforeSha) {
        this.beforeSha = beforeSha;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<String> getStages() {
        return stages;
    }
    public void setStages(ArrayList<String> stages) {
        this.stages = stages;
    }

    public String getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getFinishedAt() {
        return finishedAt;
    }
    public void setFinishedAt(String finishedAt) {
        this.finishedAt = finishedAt;
    }

    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }

    public ArrayList<Variable> getVariables() {
        return variables;
    }
    public void setVariables(ArrayList<Variable> variables) {
        this.variables = variables;
    }
}
