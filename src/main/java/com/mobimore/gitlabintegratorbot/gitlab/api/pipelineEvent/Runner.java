package com.mobimore.gitlabintegratorbot.gitlab.api.pipelineEvent;

import java.util.ArrayList;

public class Runner{
    public int id;
    public String description;
    public boolean active;
    public String runner_type;
    public boolean is_shared;
    public ArrayList<String> tags;
}
