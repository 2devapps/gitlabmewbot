package com.mobimore.gitlabintegratorbot.gitlab.api.mergeEvent;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mobimore.gitlabintegratorbot.gitlab.api.commons.User;

import java.util.ArrayList;

public class MergeRequestEvent {
    @JsonProperty("object_kind")
    public String objectKind;
    @JsonProperty("event_type")
    public String eventType;
    public User user;
    public Project project;
    public Repository repository;
    @JsonProperty("object_attributes")
    public ObjectAttributes objectAttributes;
    public ArrayList<Label> labels;
    public Changes changes;

    public String getObjectKind() {
        return objectKind;
    }
    public void setObjectKind(String objectKind) {
        this.objectKind = objectKind;
    }

    public String getEventType() {
        return eventType;
    }
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public Project getProject() {
        return project;
    }
    public void setProject(Project project) {
        this.project = project;
    }

    public Repository getRepository() {
        return repository;
    }
    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    public ObjectAttributes getObjectAttributes() {
        return objectAttributes;
    }
    public void setObjectAttributes(ObjectAttributes objectAttributes) {
        this.objectAttributes = objectAttributes;
    }

    public ArrayList<Label> getLabels() {
        return labels;
    }
    public void setLabels(ArrayList<Label> labels) {
        this.labels = labels;
    }

    public Changes getChanges() {
        return changes;
    }
    public void setChanges(Changes changes) {
        this.changes = changes;
    }
}
