package com.mobimore.gitlabintegratorbot.gitlab.api.mergeEvent;

public class Changes{
    public UpdatedById updated_by_id;
    public UpdatedAt updated_at;
    public Label labels;
}
