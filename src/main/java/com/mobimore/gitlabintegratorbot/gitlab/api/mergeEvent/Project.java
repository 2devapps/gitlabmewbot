package com.mobimore.gitlabintegratorbot.gitlab.api.mergeEvent;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Project{
    public int id;
    public String name;
    public String description;
    @JsonProperty("web_url")
    public String webUrl;
    @JsonProperty("avatar_url")
    public Object avatarUrl;
    @JsonProperty("git_ssh_url")
    public String gitSshUrl;
    @JsonProperty("git_http_url")
    public String gitHttpUrl;
    public String namespace;
    @JsonProperty("visibility_level")
    public int visibilityLevel;
    @JsonProperty("path_with_namespace")
    public String pathWithNamespace;
    @JsonProperty("default_branch")
    public String defaultBranch;
    public String homepage;
    public String url;
    @JsonProperty("ssh_url")
    public String sshUrl;
    @JsonProperty("http_url")
    public String httpUrl;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebUrl() {
        return webUrl;
    }
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public Object getAvatarUrl() {
        return avatarUrl;
    }
    public void setAvatarUrl(Object avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getGitSshUrl() {
        return gitSshUrl;
    }
    public void setGitSshUrl(String gitSshUrl) {
        this.gitSshUrl = gitSshUrl;
    }

    public String getGitHttpUrl() {
        return gitHttpUrl;
    }
    public void setGitHttpUrl(String gitHttpUrl) {
        this.gitHttpUrl = gitHttpUrl;
    }

    public String getNamespace() {
        return namespace;
    }
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public int getVisibilityLevel() {
        return visibilityLevel;
    }
    public void setVisibilityLevel(int visibilityLevel) {
        this.visibilityLevel = visibilityLevel;
    }

    public String getPathWithNamespace() {
        return pathWithNamespace;
    }
    public void setPathWithNamespace(String pathWithNamespace) {
        this.pathWithNamespace = pathWithNamespace;
    }

    public String getDefaultBranch() {
        return defaultBranch;
    }
    public void setDefaultBranch(String defaultBranch) {
        this.defaultBranch = defaultBranch;
    }

    public String getHomepage() {
        return homepage;
    }
    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String getSshUrl() {
        return sshUrl;
    }
    public void setSshUrl(String sshUrl) {
        this.sshUrl = sshUrl;
    }

    public String getHttpUrl() {
        return httpUrl;
    }
    public void setHttpUrl(String httpUrl) {
        this.httpUrl = httpUrl;
    }
}
