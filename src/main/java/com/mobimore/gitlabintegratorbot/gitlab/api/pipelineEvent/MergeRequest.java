package com.mobimore.gitlabintegratorbot.gitlab.api.pipelineEvent;

public class MergeRequest{
    public int id;
    public int iid;
    public String title;
    public String source_branch;
    public int source_project_id;
    public String target_branch;
    public int target_project_id;
    public String state;
    public String merge_status;
    public String url;
}
