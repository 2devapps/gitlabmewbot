package com.mobimore.gitlabintegratorbot.gitlab.api.mergeEvent;

import java.util.ArrayList;
import java.util.Date;

public class Label {
    public int id;
    public String title;
    public String color;
    public int project_id;
    public Date created_at;
    public Date updated_at;
    public boolean template;
    public String description;
    public String type;
    public int group_id;
    public ArrayList<Label> previous;
    public ArrayList<Label> current;
}
