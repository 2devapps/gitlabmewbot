package com.mobimore.gitlabintegratorbot.gitlab.api.mergeEvent;

public class UpdatedAt{
    public String previous;
    public String current;
}
