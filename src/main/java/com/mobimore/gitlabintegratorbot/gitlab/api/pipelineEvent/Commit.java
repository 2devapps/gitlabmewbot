package com.mobimore.gitlabintegratorbot.gitlab.api.pipelineEvent;

import com.mobimore.gitlabintegratorbot.gitlab.api.commons.Author;

import java.util.Date;

public class Commit{
    public String id;
    public String message;
    public Date timestamp;
    public String url;
    public Author author;
}
