package com.mobimore.gitlabintegratorbot.gitlab.api.mergeEvent;

import com.mobimore.gitlabintegratorbot.gitlab.api.commons.Author;

import java.util.Date;

public class LastCommit{
    public String id;
    public String message;
    public Date timestamp;
    public String url;
    public Author author;
}
