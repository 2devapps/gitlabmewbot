package com.mobimore.gitlabintegratorbot.gitlab.api.mergeEvent;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class ObjectAttributes{
    public int id;
    @JsonProperty("target_branch")
    public String targetBranch;
    @JsonProperty("source_branch")
    public String sourceBranch;
    @JsonProperty("source_project_id")
    public int sourceProjectId;
    @JsonProperty("author_id")
    public int authorId;
    @JsonProperty("assignee_id")
    public int assigneeId;
    public String title;
    @JsonProperty("created_at")
    public String createdAt;
    @JsonProperty("updated_at")
    public String updatedAt;
    @JsonProperty("milestone_id")
    public Object milestoneId;
    public String state;
    @JsonProperty("merge_status")
    public String mergeStatus;
    @JsonProperty("target_project_id")
    public int targetProjectId;
    public int iid;
    public String description;
    public Source source;
    public Target target;
    @JsonProperty("last_commit")
    public LastCommit lastCommit;
    @JsonProperty("work_in_progress")
    public boolean workInProgress;
    public String url;
    public String action;
    public Assignee assignee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTargetBranch() {
        return targetBranch;
    }

    public void setTargetBranch(String targetBranch) {
        this.targetBranch = targetBranch;
    }

    public String getSourceBranch() {
        return sourceBranch;
    }

    public void setSourceBranch(String sourceBranch) {
        this.sourceBranch = sourceBranch;
    }

    public int getSourceProjectId() {
        return sourceProjectId;
    }

    public void setSourceProjectId(int sourceProjectId) {
        this.sourceProjectId = sourceProjectId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public int getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(int assigneeId) {
        this.assigneeId = assigneeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getMilestoneId() {
        return milestoneId;
    }

    public void setMilestoneId(Object milestoneId) {
        this.milestoneId = milestoneId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMergeStatus() {
        return mergeStatus;
    }

    public void setMergeStatus(String mergeStatus) {
        this.mergeStatus = mergeStatus;
    }

    public int getTargetProjectId() {
        return targetProjectId;
    }

    public void setTargetProjectId(int targetProjectId) {
        this.targetProjectId = targetProjectId;
    }

    public int getIid() {
        return iid;
    }

    public void setIid(int iid) {
        this.iid = iid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public LastCommit getLastCommit() {
        return lastCommit;
    }

    public void setLastCommit(LastCommit lastCommit) {
        this.lastCommit = lastCommit;
    }

    public boolean isWorkInProgress() {
        return workInProgress;
    }

    public void setWorkInProgress(boolean workInProgress) {
        this.workInProgress = workInProgress;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Assignee getAssignee() {
        return assignee;
    }

    public void setAssignee(Assignee assignee) {
        this.assignee = assignee;
    }
}
