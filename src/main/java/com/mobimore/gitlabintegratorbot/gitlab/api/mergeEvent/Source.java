package com.mobimore.gitlabintegratorbot.gitlab.api.mergeEvent;

public class Source{
    public String name;
    public String description;
    public String web_url;
    public Object avatar_url;
    public String git_ssh_url;
    public String git_http_url;
    public String namespace;
    public int visibility_level;
    public String path_with_namespace;
    public String default_branch;
    public String homepage;
    public String url;
    public String ssh_url;
    public String http_url;
}
