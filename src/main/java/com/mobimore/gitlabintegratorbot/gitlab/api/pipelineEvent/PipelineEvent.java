package com.mobimore.gitlabintegratorbot.gitlab.api.pipelineEvent;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mobimore.gitlabintegratorbot.gitlab.api.commons.User;

import java.util.ArrayList;

public class PipelineEvent {
    @JsonProperty("object_kind")
    public String objectKind;
    @JsonProperty("object_attributes")
    public ObjectAttributes objectAttributes;
    @JsonProperty("merge_request")
    public MergeRequest mergeRequest;
    public User user;
    public Project project;
    public Commit commit;
    public ArrayList<Build> builds;

    public String getObjectKind() {
        return objectKind;
    }
    public void setObjectKind(String objectKind) {
        this.objectKind = objectKind;
    }

    public ObjectAttributes getObjectAttributes() {
        return objectAttributes;
    }
    public void setObjectAttributes(ObjectAttributes objectAttributes) {
        this.objectAttributes = objectAttributes;
    }

    public MergeRequest getMergeRequest() {
        return mergeRequest;
    }
    public void setMergeRequest(MergeRequest mergeRequest) {
        this.mergeRequest = mergeRequest;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public Project getProject() {
        return project;
    }
    public void setProject(Project project) {
        this.project = project;
    }

    public Commit getCommit() {
        return commit;
    }
    public void setCommit(Commit commit) {
        this.commit = commit;
    }

    public ArrayList<Build> getBuilds() {
        return builds;
    }
    public void setBuilds(ArrayList<Build> builds) {
        this.builds = builds;
    }
}
