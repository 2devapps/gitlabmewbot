package com.mobimore.gitlabintegratorbot.gitlab.api.pipelineEvent;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mobimore.gitlabintegratorbot.gitlab.api.commons.User;

public class Build {
    public int id;
    public String stage;
    public String name;
    public String status;
    @JsonProperty("created_at")
    public String createdAt;
    @JsonProperty("started_at")
    public String startedAt;
    @JsonProperty("finished_at")
    public String finishedAt;
    public String when;
    public boolean manual;
    @JsonProperty("allow_failure")
    public boolean allowFailure;
    public User user;
    public Runner runner;
    @JsonProperty("artifacts_file")
    public ArtifactsFile artifactsFile;
    public Environment environment;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getStage() {
        return stage;
    }
    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getStartedAt() {
        return startedAt;
    }
    public void setStartedAt(String startedAt) {
        this.startedAt = startedAt;
    }

    public String getFinishedAt() {
        return finishedAt;
    }
    public void setFinishedAt(String finishedAt) {
        this.finishedAt = finishedAt;
    }

    public String getWhen() {
        return when;
    }
    public void setWhen(String when) {
        this.when = when;
    }

    public boolean isManual() {
        return manual;
    }
    public void setManual(boolean manual) {
        this.manual = manual;
    }

    public boolean isAllowFailure() {
        return allowFailure;
    }
    public void setAllowFailure(boolean allowFailure) {
        this.allowFailure = allowFailure;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public Runner getRunner() {
        return runner;
    }
    public void setRunner(Runner runner) {
        this.runner = runner;
    }

    public ArtifactsFile getArtifactsFile() {
        return artifactsFile;
    }
    public void setArtifactsFile(ArtifactsFile artifactsFile) {
        this.artifactsFile = artifactsFile;
    }

    public Environment getEnvironment() {
        return environment;
    }
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
