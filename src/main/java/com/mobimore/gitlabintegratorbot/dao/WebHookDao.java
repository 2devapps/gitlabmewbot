package com.mobimore.gitlabintegratorbot.dao;

import com.mobimore.gitlabintegratorbot.domain.Hook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface WebHookDao extends JpaRepository<Hook, UUID> {
    Hook findHookById(UUID uuid);

    @Modifying
    @Query("update Hook h set h.enabled = ?1 where h.chatID = ?2")
    void enableHooksForChatId(Boolean enabled, Long chatId);

    @Modifying
    @Query("update Hook h set h.chatID = ?1 where h.chatID = ?2")
    void updateChatId(Long newChatId, Long oldChatId);
}
