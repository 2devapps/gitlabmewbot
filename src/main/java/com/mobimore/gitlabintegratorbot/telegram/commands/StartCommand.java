package com.mobimore.gitlabintegratorbot.telegram.commands;

import com.mobimore.gitlabintegratorbot.dao.WebHookDao;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

@Component
public class StartCommand extends BotCommand {

    private final WebHookDao webHookDao;

    public StartCommand(WebHookDao webHookDao) {
        super("start", "Start with bot with this command");
        this.webHookDao = webHookDao;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        if (chat != null) {
            webHookDao.enableHooksForChatId(true, chat.getId());
        }
    }
}
