package com.mobimore.gitlabintegratorbot.telegram.commands;

import com.mobimore.gitlabintegratorbot.configuration.ApiConfiguration;
import com.mobimore.gitlabintegratorbot.dao.WebHookDao;
import com.mobimore.gitlabintegratorbot.domain.Hook;
import com.mobimore.gitlabintegratorbot.telegram.GitlabMewBot;
import com.mobimore.gitlabintegratorbot.utils.HTML;
import com.mobimore.gitlabintegratorbot.webhookservice.GitlabWebhooksRestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Date;
import java.util.UUID;

@Component
public class NewHookCommand extends BotCommand {
    private final ApiConfiguration apiConfiguration;
    private final WebHookDao webHookDao;

    private final Logger logger = LoggerFactory.getLogger(GitlabWebhooksRestController.class);

    public NewHookCommand(WebHookDao webHookDao, ApiConfiguration apiConfiguration) {
        super("newhook", "Creates new hook id and registers it to current chat");
        this.webHookDao = webHookDao;
        this.apiConfiguration = apiConfiguration;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {
        Long chatId = chat.getId();
        UUID hookUUID = UUID.randomUUID();

        SendMessage.SendMessageBuilder messageBuilder = SendMessage.builder();
        messageBuilder.parseMode("html");
        messageBuilder.chatId(chat.getId().toString());

        String url = apiConfiguration.getFullApiUrl() + hookUUID;
        HTML html = new HTML();
        html.text("Your Gitlab hook URL is: ").a(url, url)
                .text("\n Add it to your Gitlab hooks.")
                .text("\n I only support ").b("merge").text(" and ").b("pipeline").text(" events");

        messageBuilder.text(html.toString());

        Hook hook = new Hook();
        hook.setId(hookUUID);
        hook.setChatID(chatId);
        hook.setOwnerID(user.getId());
        hook.setCreateDate(new Date());
        hook.setEnabled(true);
        webHookDao.save(hook);
        try {
            absSender.execute(messageBuilder.build());
        } catch (TelegramApiException e) {
            logger.error("Can't send message to chat ID: " + chatId, e);
        }
    }
}
