package com.mobimore.gitlabintegratorbot.telegram;

import com.mobimore.gitlabintegratorbot.telegram.commands.NewHookCommand;
import com.mobimore.gitlabintegratorbot.telegram.commands.StartCommand;
import com.mobimore.gitlabintegratorbot.telegram.commands.StopCommand;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class GitlabMewBot extends TelegramLongPollingCommandBot {
    @Value("${bot.token}")
    private String botToken;

    @Value("${bot.name}")
    private  String botName;

    public GitlabMewBot(NewHookCommand newHookCommand, StartCommand startCommand, StopCommand stopCommand) {
        register(startCommand);
        register(newHookCommand);
        register(stopCommand);
    }

    @Override
    public String getBotUsername() {
        return botName;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public void processNonCommandUpdate(Update update) {
        // Don't answer to non-command messages
    }
}
