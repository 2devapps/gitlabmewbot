package com.mobimore.gitlabintegratorbot.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import java.util.Date;
import java.util.UUID;

@Entity
public class Hook {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @Column(name = "chat_ID", columnDefinition = "BIGINT(20)", nullable = false)
    private Long chatID;

    @Column(name = "keyphrase")
    private String keyphrase;

    @Column(name = "owner_ID", columnDefinition = "BIGINT(20)", nullable = false)
    private Long ownerID;

    @Column(name = "create_date", nullable = false)
    private Date createDate;

    @Column(name = "update_date")
    private Date updateDate;

    @Column(name = "last_use_date")
    private Date lastUseDate;

    @PreUpdate
    private void preUpdate() {
        updateDate = new Date();
    }

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public Boolean getEnabled() {
        return enabled;
    }
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Long getChatID() {
        return chatID;
    }
    public void setChatID(Long chatID) {
        this.chatID = chatID;
    }

    public String getKeyphrase() {
        return keyphrase;
    }
    public void setKeyphrase(String keyphrase) {
        this.keyphrase = keyphrase;
    }

    public Long getOwnerID() {
        return ownerID;
    }
    public void setOwnerID(Long ownerID) {
        this.ownerID = ownerID;
    }

    public Date getCreateDate() {
        return createDate;
    }
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getLastUseDate() {
        return lastUseDate;
    }
    public void setLastUseDate(Date lastUseDate) {
        this.lastUseDate = lastUseDate;
    }
}
