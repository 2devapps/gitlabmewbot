package com.mobimore.gitlabintegratorbot.webhookservice;

import com.mobimore.gitlabintegratorbot.gitlab.api.mergeEvent.MergeRequestEvent;
import com.mobimore.gitlabintegratorbot.gitlab.api.pipelineEvent.PipelineEvent;
import com.mobimore.gitlabintegratorbot.webhookservice.processor.WebhookEventsProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GitlabWebhooksRestController {

    private final Logger logger = LoggerFactory.getLogger(GitlabWebhooksRestController.class);

    private final WebhookEventsProcessor processor;

    public GitlabWebhooksRestController(WebhookEventsProcessor processor) {
        this.processor = processor;
    }

    @PostMapping(value = "gitlab/{uuid}", headers = "X-Gitlab-Event=Merge Request Hook", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> mergeRequestEvent(@RequestBody MergeRequestEvent mergeRequestEvent, @PathVariable String uuid) {
        logger.debug("Received merge request event. UUID: " + uuid);
        if (ObjectUtils.isEmpty(uuid) || ObjectUtils.isEmpty(mergeRequestEvent)) {
            logger.error("Error processing merge request event. UUID: " + uuid + " event: " + mergeRequestEvent);
            return ResponseEntity.badRequest().build();
        }
        processor.processMergeRequestEvent(mergeRequestEvent, uuid);
        logger.debug("Merge request event processed");
        return ResponseEntity.ok().body("Merge request event processed");
    }

    @PostMapping(value = "gitlab/{uuid}", headers = "X-Gitlab-Event=Pipeline Hook", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> pipelineEvent(@RequestBody PipelineEvent pipelineEvent, @PathVariable String uuid) {
        logger.debug("Received pipeline event. UUID:" + uuid);
        if (ObjectUtils.isEmpty(uuid) || ObjectUtils.isEmpty(pipelineEvent)) {
            logger.error("Error processing pipeline event. UUID: " + uuid + " event: " + pipelineEvent);
            return ResponseEntity.badRequest().build();
        }
        processor.processPipelineEvent(pipelineEvent, uuid);
        logger.debug("Pipeline event processed");
        return ResponseEntity.ok().body("Pipeline event processed");
    }
}
