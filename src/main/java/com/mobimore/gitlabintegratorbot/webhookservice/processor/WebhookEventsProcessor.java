package com.mobimore.gitlabintegratorbot.webhookservice.processor;

import com.mobimore.gitlabintegratorbot.dao.WebHookDao;
import com.mobimore.gitlabintegratorbot.domain.Hook;
import com.mobimore.gitlabintegratorbot.gitlab.api.commons.User;
import com.mobimore.gitlabintegratorbot.gitlab.api.mergeEvent.MergeRequestEvent;
import com.mobimore.gitlabintegratorbot.gitlab.api.mergeEvent.ObjectAttributes;
import com.mobimore.gitlabintegratorbot.gitlab.api.mergeEvent.Project;
import com.mobimore.gitlabintegratorbot.gitlab.api.pipelineEvent.PipelineEvent;
import com.mobimore.gitlabintegratorbot.telegram.GitlabMewBot;
import com.mobimore.gitlabintegratorbot.utils.HTML;
import com.mobimore.gitlabintegratorbot.webhookservice.GitlabWebhooksRestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.ResponseParameters;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import javax.annotation.Nonnull;
import java.util.UUID;

@Component
@Transactional
public class WebhookEventsProcessor {
    private final GitlabMewBot gitlabMewBot;
    private final WebHookDao webHookDao;

    private final Logger logger = LoggerFactory.getLogger(GitlabWebhooksRestController.class);

    public WebhookEventsProcessor(GitlabMewBot gitlabMewBot, WebHookDao webHookDao) {
        this.gitlabMewBot = gitlabMewBot;
        this.webHookDao = webHookDao;
    }

    @Async
    public void processMergeRequestEvent(@Nonnull MergeRequestEvent mergeRequestEvent, @Nonnull String uuid) {
        UUID uuidObj = UUID.fromString(uuid);
        Hook hookById = webHookDao.findHookById(uuidObj);
        if (hookById == null) {
            logger.error("Hook not found for UUID: " + uuid);
            return;
        }

        User user = mergeRequestEvent.getUser();
        String username = user != null ? user.getUsername() : "unknown_user";

        ObjectAttributes objectAttributes = mergeRequestEvent.getObjectAttributes();
        if (objectAttributes != null) {
            String state = objectAttributes.getState() != null ? objectAttributes.getState() : "unknown_state";
            String action = objectAttributes.getAction() != null ? objectAttributes.getAction() : "unknown_action";
            int mergeRequestId = objectAttributes.getIid();
            String title = objectAttributes.getTitle();
            String description = objectAttributes.getDescription();
            Project project = mergeRequestEvent.getProject();
            String sourceBranch = objectAttributes.getSourceBranch();
            String targetBranch = objectAttributes.getTargetBranch();
            if (project != null) {
                String projectName = project.getName();
                String projectWebUrl = project.getWebUrl();

                HTML html = new HTML();
                html.b(username);
                if ("update".equals(action)) {
                    html.text(" updated ");
                } else {
                    html.text(" " + state + " ");
                }
                html.a(projectWebUrl + "/-/merge_requests/" + mergeRequestId, "request")
                        .text(" to merge ").b(sourceBranch).text(" in ").b(targetBranch).text(" at ").a(projectWebUrl, projectName).text(":\n")
                        .text(title != null ? (title + '\n') : "")
                        .text(description != null ? description : "");
                try {
                    sendHTMLTelegramMessage(hookById.getChatID(), html);
                } catch (Exception e) {
                    logger.error("Can't send message for UUID: " + uuid, e);
                }
            } else {
                logger.error("Project is null for UUID: " + uuid);
                logger.debug("Request: " + mergeRequestEvent);
            }
        } else {
            logger.error("ObjectAttributes is null for UUID: " + uuid);
            logger.debug("Request: " + mergeRequestEvent);
        }
    }

    @Async
    public void processPipelineEvent(@Nonnull PipelineEvent pipelineEvent, @Nonnull String uuid) {
        Hook hookById = webHookDao.findHookById(UUID.fromString(uuid));
        if (hookById == null) {
            logger.error("Hook not found for UUID: " + uuid);
            return;
        }

        com.mobimore.gitlabintegratorbot.gitlab.api.pipelineEvent.ObjectAttributes objectAttributes = pipelineEvent.getObjectAttributes();
        if (objectAttributes != null) {
            String branch = objectAttributes.getRef();
            String status = objectAttributes.getStatus();
            int pipelineId = objectAttributes.getId();

            com.mobimore.gitlabintegratorbot.gitlab.api.pipelineEvent.Project project = pipelineEvent.getProject();
            if (project != null) {
                String projectName = project.getName();
                String projectWebUrl = project.getWebUrl();

                HTML html = new HTML();
                html.text("Pipeline for ").a(projectWebUrl, projectName).text(", branch: ").a(projectWebUrl + "/-/pipelines/" + pipelineId, branch).text(" " + status);
                try {
                    sendHTMLTelegramMessage(hookById.getChatID(), html);
                } catch (Exception e) {
                    logger.error("Can't send message for UUID: " + uuid, e);
                }
            } else {
                logger.error("Project is null for UUID: " + uuid);
                logger.debug("Request: " + pipelineEvent);
            }
        } else {
            logger.error("ObjectAttributes is null for UUID: " + uuid);
            logger.debug("Request: " + pipelineEvent);
        }
    }

    private void sendHTMLTelegramMessage(Long chatId, HTML html) throws TelegramApiException {
        SendMessage.SendMessageBuilder messageBuilder = SendMessage.builder();
        messageBuilder.parseMode("html")
                .disableWebPagePreview(true)
                .chatId(Long.toString(chatId))
                .text(html.toString());
        try {
            gitlabMewBot.execute(messageBuilder.build());
        } catch (TelegramApiRequestException e) {
            ResponseParameters parameters = e.getParameters();
            if (parameters != null) {
                Long migrateToChatId = parameters.getMigrateToChatId();
                if (migrateToChatId != null) {
                    webHookDao.updateChatId(migrateToChatId, chatId);
                    logger.debug("Chat id updated from: " + chatId + " to: " + migrateToChatId + ". Retrying sending.");
                    messageBuilder.chatId(Long.toString(migrateToChatId));
                    gitlabMewBot.execute(messageBuilder.build());
                } else {
                    throw e;
                }
            } else {
                throw e;
            }
        }
    }
}
